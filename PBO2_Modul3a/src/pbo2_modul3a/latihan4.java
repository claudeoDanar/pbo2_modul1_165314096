/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbo2_modul3a;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author ASUS
 */
public class latihan4 extends JFrame {
    private static final int FRAME_WIDTH = 400;     
    private static final int FRAME_HEIGHT = 200;     
    private static final int FRAME_X_ORIGIN = 600;   
    private static final int FRAME_Y_ORIGIN = 200;  
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 30;
    private JLabel keyword ;
    private JTextField isiKey;
    private JButton find;
    
    
    public static void main(String[] args) {
        latihan4 frame = new latihan4();
        frame.setVisible(true);
    }
    public latihan4 () {
    
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("Find");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
        
        contentPane.setLayout(null);
        contentPane.setBackground(Color.LIGHT_GRAY);
        
        
        
        keyword = new JLabel("Keyword : ");
        keyword.setBounds(175, 15, 100, 30);
        contentPane.add(keyword);
        
        find = new JButton("FIND");
        find.setBounds(163, 100, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(find);
        
        isiKey = new JTextField();
        isiKey.setBounds(10, 53, 360, 30);
        contentPane.add(isiKey);
        

    }
    

}
