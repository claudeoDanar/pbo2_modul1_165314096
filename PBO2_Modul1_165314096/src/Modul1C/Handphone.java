package Modul1C;

/**
 *
 * @author ASUS
 */
public class Handphone extends Gadget {
    
    private String rom;
    private String konektivitas;

    public Handphone() {
    }
    
    public Handphone(String ram, String konektivitas) {
        this.rom = rom;
        this.konektivitas = konektivitas;
    }

    public String getRom() {
        
        return rom;
    }

    public void setRom(String ram) {
        this.rom = rom;
    }

    public String getKonektivitas() {
        return konektivitas;
    }

    public void setKonektivitas(String konektivitas) {
        this.konektivitas = konektivitas;
    }
}
