package Modul1C;

/**
 *
 * @author ASUS
 */
public class Laptop extends Gadget{
    private String hardisk;
    private int jumlahUSB;

    public Laptop() {
    }

    public Laptop(String hardisk, int jumlahUSB) {
        this.hardisk = hardisk;
        this.jumlahUSB = jumlahUSB;
    }

    public String getHardisk() {
        return hardisk;
    }

    public void setHardisk(String hardisk) {
        this.hardisk = hardisk;
    }

    public int getJumlahUSB() {
        return jumlahUSB;
    }

    public void setJumlahUSB(int jumlahUSB) {
        this.jumlahUSB = jumlahUSB;
    }
    
    
}
