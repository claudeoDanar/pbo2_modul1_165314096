package Modul1C;

/**
 *
 * @author ASUS
 */
public class Mahasiswa extends Laptop{
    private String nim;
    private String nama;
    private Handphone handpone;
    private Tablet tablet;
    private Laptop laptop;

    public Mahasiswa() {
    }

    public Mahasiswa(String nim, String nama, Handphone handpone, Tablet tablet, Laptop laptop) {
        this.nim = nim;
        this.nama = nama;
        this.handpone = handpone;
        this.tablet = tablet;
        this.laptop = laptop;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Handphone getHandpone() {
        return handpone;
    }

    public void setHandpone(Handphone handpone) {
        this.handpone = handpone;
    }

    public Tablet getTablet() {
        return tablet;
    }

    public void setTablet(Tablet tablet) {
        this.tablet = tablet;
    }

    public Laptop getLaptop() {
        return laptop;
    }

    public void setLaptop(Laptop laptop) {
        this.laptop = laptop;
    }
    
    
}
