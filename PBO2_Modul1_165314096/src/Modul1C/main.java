package Modul1C;

/**
 *
 * @author ASUS
 */
public class main {
    
    public static void main(String[] args) {
        Handphone hp = new Handphone("2 GB", "GSM, HSPA, LTE");
        hp.setMerk("Asus");
        hp.setWarna("Putih");
        hp.setProsesor("Intel Atom Z3560");
        hp.setRam("2 GB");
    
        Tablet tb = new Tablet("8 GB", "wifi");
        tb.setMerk("Samsung");
        tb.setWarna("Hitam");
        tb.setProsesor("Marvell PXA1088");
        tb.setRam("1,5 GB");
        
        Laptop lpt = new Laptop("160 GB", 3);
        lpt.setMerk("Lenovo ThinkPad T400");
        lpt.setWarna("Hitam");
        lpt.setProsesor("Core 2 Duo 2.53 GHz");
        lpt.setRam("2 GB");
        
        
        
        Mahasiswa mhs = new Mahasiswa("165314096", "Claudeo", hp, tb, lpt);
        System.out.println("Nama : "+mhs.getNama());
        System.out.println("NIM : "+mhs.getNim());
        
        
        
       
    }
    
}
