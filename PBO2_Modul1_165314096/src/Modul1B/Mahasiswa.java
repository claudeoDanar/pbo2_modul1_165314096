/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1B;

/**
 *
 * @author ASUS
 */
public class Mahasiswa extends Penduduk {
    private String nim;

    public Mahasiswa() {
    }

    public Mahasiswa(String nim, String nama, String tempatTanggalLahir) {
        super(nama, tempatTanggalLahir);
        this.nim = nim;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    @Override
    public double hitungIuran() {
       return Double.parseDouble(nim)/10000;
    }
    
    
}
