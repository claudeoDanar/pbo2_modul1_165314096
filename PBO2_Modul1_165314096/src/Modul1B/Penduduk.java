/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1B;

/**
 *
 * @author ASUS
 */
public abstract class Penduduk {
    private String nama;
    private String tempatTanggalLahir;

    public Penduduk() {
    }

    public Penduduk(String nama, String tempatTanggalLahir) {
        this.nama = nama;
        this.tempatTanggalLahir = tempatTanggalLahir;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTempatTanggalLahir() {
        return tempatTanggalLahir;
    }

    public void setTempatTanggalLahir(String tempatTanggalLahir) {
        this.tempatTanggalLahir = tempatTanggalLahir;
    }
    
    public abstract double hitungIuran();
            }
