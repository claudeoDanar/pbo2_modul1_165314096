/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1B;

import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class UKM {
    private String namaUnit;
    private Mahasiswa ketua;
    private Mahasiswa sekretaris;
    private ArrayList<Penduduk> anggota;

    public UKM() {
        this.anggota = new ArrayList<>();
    }

    public UKM(String namaUnit) {
        this.anggota = new ArrayList<>();
        this.namaUnit = namaUnit;
    }

    public String getNamaUnit() {
        return namaUnit;
    }

    public void setNamaUnit(String namaUnit) {
        this.namaUnit = namaUnit;
    }

    public Mahasiswa getKetua() {
        return ketua;
    }

    public void setKetua(Mahasiswa ketua) {
        this.ketua = ketua;
    }

    public Mahasiswa getSekretaris() {
        return sekretaris;
    }

    public void setSekretaris(Mahasiswa sekretaris) {
        this.sekretaris = sekretaris;
    }

    public ArrayList<Penduduk> getAnggota() {
        return anggota;
    }

    public void setAnggota(Penduduk anggotaBaru) {
        anggota.add(anggotaBaru);
    }

    
}
