/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1B;

import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class PBO2_Modul1_165314096 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        UKM ukm = new UKM("UKM Sepak Takraw");
        Mahasiswa kta= new Mahasiswa("10003", "Claudeo", "Metro,09/12/1997");
        ukm.setKetua(kta);
        Mahasiswa skrt = new Mahasiswa("10002", "David", "Yogyakarta,02/07/1998");
        ukm.setSekretaris(skrt);
       
        Mahasiswa agt1 =  new Mahasiswa("10003", "Hervin", "Palembang,30/02/1998");
        ukm.setAnggota(agt1);
        
        MasyarakatSekitar msyrkt = new MasyarakatSekitar("21109", "Paidi", "Metro,32/04/1992");
        ukm.setAnggota(msyrkt);
        
        ArrayList<Penduduk> Anggota = ukm.getAnggota();
        
        System.out.println("Nama Ketua      : "+ukm.getKetua().getNama()+" \t Iuran    = "+ukm.getKetua().hitungIuran()+" ");
        System.out.println("Nama Sekertaris : "+ukm.getSekretaris().getNama()+ "\t Iuran = "+ukm.getSekretaris().hitungIuran()+" ");
        System.out.println("Anggota: ");
        double temp = 0 ;
        temp += ukm.getKetua().hitungIuran()+ukm.getSekretaris().hitungIuran();
        for (int i = 0; i < Anggota.size(); i++) {
            System.out.println("Nama :    "+Anggota.get(i).getNama()+"    Iuran =  "+" "+Anggota.get(i).hitungIuran());
            temp += Anggota.get(i).hitungIuran();
            
        }
        System.out.println("totalIuran = "+temp);
        
    }
    
}
