/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul1B;

/**
 *
 * @author ASUS
 */
public class MasyarakatSekitar extends Penduduk {
    private String nomor;

    public MasyarakatSekitar() {
    }

    public MasyarakatSekitar(String nomor, String nama, String tempatTanggalLahir) {
        super(nama, tempatTanggalLahir);
        this.nomor = nomor;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }
    

    @Override
    public double hitungIuran() {
       return Double.parseDouble(nomor)*100;
    }
}
